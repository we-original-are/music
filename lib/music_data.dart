import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_media_notification/flutter_media_notification.dart';
import 'package:path_provider/path_provider.dart';

class MusicData {
  String title = "";
  String singer = "";
  String musicUrl = "";

  MusicData({this.title, this.singer, this.musicUrl});
}

getMusicList() {
  return _musicList;
}

const String singer = "unknown";

List<MusicData> _musicList = [
  MusicData(
      title: "Relaxing-1",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/0bikalam%20download1music.ir%20(1).mp3"),
  MusicData(
      title: "Relaxing-2",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/0bikalam%20download1music.ir%20(10).mp3"),
  MusicData(
      title: 'Relaxing-3',
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/0bikalam%20download1music.ir%20(11).mp3"),
  MusicData(
      title: "Relaxing-4",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/0bikalam%20download1music.ir%20(12).mp3"),
  MusicData(
      title: "Relaxing-5",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/0bikalam%20download1music.ir%20(2).mp3"),
  MusicData(
      title: "Relaxing-6",
      singer: singer,
      musicUrl: "https://dl.download1music.ir/Music/Without-Words/Autumn.mp3"),
  MusicData(
      title: "Relaxing-7",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/Crying-Red-Spot.mp3"),
  MusicData(
      title: "Relaxing-8",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/Devakant-Nature-Spirits.mp3"),
  MusicData(
      title: "Relaxing-9",
      singer: singer,
      musicUrl: "https://dl.download1music.ir/Music/Without-Words/Earth.mp3"),
  MusicData(
      title: "Relaxing-10",
      singer: singer,
      musicUrl: "https://dl.download1music.ir/Music/Without-Words/musica.mp3"),
  MusicData(
      title: "Relaxing-11",
      singer: singer,
      musicUrl: "https://dl.download1music.ir/Music/Without-Words/Snow.mp3"),
  MusicData(
      title: "Relaxing-12",
      singer: singer,
      musicUrl:
          "https://dl.download1music.ir/Music/Without-Words/SoheilHosnavi-Uranus.mp3"),
];

/// /////////////////////////////////
///

Future getMusicDirectory() async {
 Directory directory= await getExternalStorageDirectory();
  return directory;
}

showNotificationWhenPlaying(String title, String singer) async {
  MediaNotification.showNotification(
      title: title, author: singer, isPlaying: true);
}

showNotificationWhenPausing(String title, String singer) async {
  MediaNotification.showNotification(
      title: title, author: singer, isPlaying: false);
}

hideNotification() async {
  MediaNotification.hideNotification();
}

Widget downloadingDesign(bool loadingDownload, double progress, int newProgress,
    BuildContext context,String currentSingerDl, String currentTitleDl) {
  double widthX = MediaQuery.of(context).size.width;
  return Align(
    alignment: Alignment.topCenter,
    child: Container(
        color: Colors.white,
        width: widthX,
        child: loadingDownload
            ? Stack(
                children: [
                  LinearProgressIndicator(
                    color:Colors.green,
                    minHeight: 30,
                    value: progress,
                    backgroundColor: Theme.of(context).primaryColorLight,
                  ),
                  SizedBox(
                    width: widthX,
                    child: Text(
                      "% $newProgress | $currentTitleDl - $currentSingerDl ",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Theme.of(context).primaryIconTheme.color),
                    ),
                  )
                ],
              )
            : const Text("")),
  );
}
