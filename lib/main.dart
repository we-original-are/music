import 'dart:async';
import 'package:flutter/material.dart';

import 'home_screen.dart';
import 'splashscreen.dart';

void main() {
  runApp( const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);


  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String title = "Music";

  bool showSplash = true;

  void startTime() async {
    Timer(const Duration(seconds: 8), () {
      setState(() {
        showSplash = false;
      });
    });
  }

  @override
  void initState() {
    startTime();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: title,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "fontVazir"
      ),
      home: showSplash ?  SplashScreen() : const HomePage(),
    );
  }
}
