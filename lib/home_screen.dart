import 'dart:async';
import 'dart:io';
import 'package:audioplayer/audioplayer.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'Custom/custom_list_tile.dart';
import 'package:flutter_media_notification/flutter_media_notification.dart';
import 'music_data.dart';

enum PlayerState { stopped, playing, paused }

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  AudioPlayer audioPlayer = AudioPlayer();
  Dio dio = Dio();

  double progress = 0.0;
  int newProgress = 0;

  bool loadingDownload = false;

  String localFilePath = "";

  int currentIndex = 0;

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;
  bool playerOn = false;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  List<MusicData> list = getMusicList();
  Duration duration = const Duration();
  Duration position = const Duration();

  String currentTitleDl = "";
  String currentSingerDl = "";

  String currentTitle = "";
  String currentSinger = "";
  String currentMusicUrl = "";
  String currentCover = "assets/picMusic.jpg";

  @override
  void initState() {
    hideNotification();
    initAudioPlayer();
    setupNotification();
    super.initState();
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    hideNotification();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (position != null &&
        duration != null &&
        position > const Duration(seconds: 1) &&
        position == duration) {
      setState(() {
        nextAction();
      });
    }
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(Icons.library_music),
        title: const Text("Music"),
      ),
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.blue, Colors.tealAccent],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight)),
        child: Column(
          children: [
            loadingDownload
                ? Container(
                    child: newProgress == 100
                        ? const Text(
                            "",
                            style: TextStyle(fontSize: 0.0),
                          )
                    //TODO:
                        : downloadingDesign(
                            loadingDownload, progress, newProgress, context,currentSingerDl,currentTitleDl))
                : const Text(
                    "",
                    style: TextStyle(fontSize: 0.0),
                  ),
            Expanded(
                child: Scrollbar(
              child: ListView.separated(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return customListTile(
                    onTap: () {
                      musicDialog(index);
                    },
                    title: list[index].title,
                    singer: list[index].singer,
                    cover: "assets/picMusic.jpg",
                    currentIndex: currentIndex,
                    musicList: list,
                    playerOn: playerOn,
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const Divider(
                    color: Colors.white,
                    indent: 8.0,
                    endIndent: 8.0,
                  );
                },
              ),
            )),
            playerOn
                ? Container(
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0)),
                        image: DecorationImage(
                            image: AssetImage(currentCover), fit: BoxFit.cover),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(color: Colors.white, blurRadius: 10.0),
                        ]),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.black.withOpacity(.3),
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(10.0),
                            topRight: Radius.circular(10.0)),
                      ),
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            Icons.multitrack_audio,
                                            color: Colors.white,
                                          ),
                                          const SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "$currentTitle - $currentSinger"
                                                        .length >
                                                    40
                                                ? "$currentTitle - $currentSinger"
                                                        .substring(0, 40) +
                                                    " ..."
                                                : "$currentTitle - $currentSinger",
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      IconButton(
                                          tooltip: "Close",
                                          color: Colors.white,
                                          onPressed: () {
                                            setState(() {
                                              playerOn = false;
                                              stop();
                                              hideNotification();
                                            });
                                          },
                                          icon: const Icon(Icons.close))
                                    ],
                                  ),
                                ),
                                Slider(
                                    thumbColor: Colors.white,
                                    activeColor: Colors.white,
                                    inactiveColor: Colors.white38,
                                    value: position.inMilliseconds.toDouble(),
                                    onChanged: (double value) {
                                      return audioPlayer
                                          .seek((value / 1000).roundToDouble())
                                          .ignore();
                                    },
                                    min: 0.0,
                                    max: duration.inMilliseconds.toDouble() +
                                        1000.toDouble()),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      if (position != null)
                                        Padding(
                                          padding: const EdgeInsets.all(3.0),
                                          child: Text(
                                            position != null
                                                ? "${positionText ?? ''}"
                                                : '',
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      if (position != null)
                                        Padding(
                                          padding: const EdgeInsets.all(3.0),
                                          child: Text(
                                            duration != null
                                                ? durationText
                                                : '',
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 8.0,
                                ),
                                musicPlayerController(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : const Text("")
          ],
        ),
      ),
    );
  }

  musicDialog(int index) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            titlePadding: EdgeInsets.zero,
            title: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(10),
                      topLeft: Radius.circular(10))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Music Options",
                    style: TextStyle(color: Colors.white),
                  ),
                  CloseButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    color: Colors.white,
                  )
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    setCurrentMusicData("", index);
                  },
                  child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("play online"))),
              TextButton(
                  onPressed: () async {
                    Directory directory = await getMusicDirectory();
                    if (await directory.exists()) {
                      File file =
                          File(directory.path + "${list[index].title}.mp3");
                      if (await file.exists()) {
                        Navigator.pop(context);
                        setCurrentMusicData(file.path, index);
                      } else {
                        Navigator.pop(context);
                        showDownloadDialog(index);
                      }
                    } else {
                      Navigator.pop(context);
                      showDownloadDialog(index);
                    }
                  },
                  child: const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text("play offline"),
                  ))
            ],
          );
        });
  }

  void showDownloadDialog(int index) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            titlePadding: EdgeInsets.zero,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            title: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(10),
                      topLeft: Radius.circular(10))),
              child: const Text(
                "music not found!",
                style: TextStyle(color: Colors.white),
              ),
            ),
            content: const Text("Are you want download this music?"),
            actions: [
              TextButton(
                  onPressed: () async {
                    if (loadingDownload) {
                      Navigator.pop(context);
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text(
                            'Download is currently please wait ... ',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      );
                    } else {
                      Navigator.pop(context);
                      setState(() {
                        currentTitleDl = list[index].title;
                        currentSingerDl = list[index].singer;
                      });
                      await downloadFile(
                          list[index].title, list[index].musicUrl);
                    }
                  },
                  child: const Text("Download")),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("Cancel")),
            ],
          );
        });
  }

  setCurrentMusicData(String path, int index) {
    setState(() {
      playerOn = true;
      currentIndex = index;
      currentTitle = list[index].title;
      currentSinger = list[index].singer;
      currentMusicUrl = list[index].musicUrl;
      stop();
      showNotificationWhenPlaying(currentTitle, currentSinger);
    });

    if (path.isNotEmpty) {
      setState(() {
        localFilePath = path;
        _playLocal();
      });
    } else {
      setState(() {
        play(list[index].musicUrl);
      });
    }
  }

  Widget musicPlayerController() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            isMuted ? mute(false) : mute(true);
          },
          icon: isMuted
              ? const Icon(
                  Icons.volume_off,
                  color: Colors.white,
                )
              : const Icon(
                  Icons.volume_up,
                  color: Colors.white,
                ),
        ),
        IconButton(
            color: Colors.white,
            onPressed: () {
              previousAction();
            },
            icon: const Icon(Icons.skip_previous)),
        IconButton(
          color: Colors.white,
          onPressed: () {
            if (isPlaying || isPaused) {
              stop();
              setNotificationPlayAndPauseIcon();
            }
          },
          icon: const Icon(Icons.stop),
        ),
        IconButton(
            color: Colors.white,
            onPressed: () {
              playAndPauseAction();
            },
            icon: isPlaying
                ? const Icon(Icons.pause)
                : const Icon(Icons.play_arrow)),
        IconButton(
            color: Colors.white,
            onPressed: () {
              nextAction();
            },
            icon: const Icon(Icons.skip_next)),
      ],
    );
  }

  setNotificationPlayAndPauseIcon() async {
    isPlaying
        ? showNotificationWhenPausing(currentTitle, currentSinger)
        : showNotificationWhenPlaying(currentTitle, currentSinger);
  }

  playAndPauseAction() async {
    Directory directory = await getMusicDirectory();
    final file = File(directory.path + "${list[currentIndex].title}.mp3");
    if (await file.exists()) {
      setState(() {
        localFilePath = file.path;
      });
      setNotificationPlayAndPauseIcon();
      isPlaying ? pause() : _playLocal();
    } else {
      setNotificationPlayAndPauseIcon();
      isPlaying ? pause() : play(currentMusicUrl);
    }
  }

  nextAction() async {
    if (currentIndex == list.length - 1) {
      setState(() {
        currentIndex = 0;
        currentTitle = list[currentIndex].title;
        currentSinger = list[currentIndex].singer;
        currentMusicUrl = list[currentIndex].musicUrl;
        stop();
        playAndPauseAction();
      });
    } else {
      setState(() {
        currentIndex++;
        currentTitle = list[currentIndex].title;
        currentSinger = list[currentIndex].singer;
        currentMusicUrl = list[currentIndex].musicUrl;
        stop();
        playAndPauseAction();
      });
    }
  }

  previousAction() async {
    if (currentIndex == 0) {
      setState(() {
        currentIndex = list.length - 1;
        currentTitle = list[currentIndex].title;
        currentSinger = list[currentIndex].singer;
        currentMusicUrl = list[currentIndex].musicUrl;
        stop();
        playAndPauseAction();
      });
    } else {
      setState(() {
        currentIndex--;
        currentTitle = list[currentIndex].title;
        currentSinger = list[currentIndex].singer;
        currentMusicUrl = list[currentIndex].musicUrl;
        stop();
        playAndPauseAction();
      });
    }
  }

  void setupNotification() {
    MediaNotification.setListener('play', () {
      setState(() {
        playAndPauseAction();
      });
    });

    MediaNotification.setListener('pause', () {
      setState(() {
        playAndPauseAction();
      });
    });

    MediaNotification.setListener('next', () {
      setState(() {
        nextAction();
      });
    });

    MediaNotification.setListener('prev', () {
      setState(() {
        previousAction();
      });
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  /// ///////////////////////////////////////////////////
  ///
  /// TODO: is here, music download and music save methods.
  ///
  /// ///////////////////////////////////////////////////

  Future saveFile(String fileName, String url) async {
    try {
      Directory directory = await getMusicDirectory();
      if (await directory.exists()) {
        File file = File(directory.path + fileName);
        await dio.download(url, file.path,
            onReceiveProgress: (downloaded, totalSize) {
          setState(() {
            progress = downloaded / totalSize;
            newProgress = (progress * 100).toInt();
          });
        });
        return true;
      } else {
        await directory.create(recursive: true);
        File file = File(directory.path + fileName);
        await dio.download(url, file.path,
            onReceiveProgress: (downloaded, totalSize) {
          setState(() {
            progress = downloaded / totalSize;
            newProgress = (progress * 100).toInt();
          });
        });
        return true;
      }
    } catch (error) {
      return false;
    }
  }

  Future downloadFile(String musicTitle, String url) async {
    setState(() {
      loadingDownload = true;
    });
    bool downloaded = await saveFile("$musicTitle.mp3", url);
    if (downloaded) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Downloaded Successfully',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Download Error',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
    }
  }

  ///
  /// /////////////////////////////////

  Future play(String url) async {
    try {
      await audioPlayer.play(url);
      setState(() {
        playerState = PlayerState.playing;
      });
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'Connection error',
            style: TextStyle(color: Colors.white),
          ),
        ),
      );
    }
  }

  Future _playLocal() async {
    await audioPlayer.play(localFilePath, isLocal: true);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() {
      playerState = PlayerState.paused;
    });
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = const Duration();
    });
  }

  void initAudioPlayer() async {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = const Duration(seconds: 0);
        position = const Duration(seconds: 0);
      });
    });
  }
}
